package com.levelup.example;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Example {

    private int value;

    public int sum(int a, int b){
        return a + b;
    }

}
