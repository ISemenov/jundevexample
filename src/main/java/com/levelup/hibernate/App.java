package com.levelup.hibernate;

import com.levelup.hibernate.domain.Engineer;
import com.levelup.hibernate.domain.Subject;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;

/**
 * Created by Student-28 on 18.01.2019.
 */
public class App {

    public static void main(String[] args) {

        HibernateConfiguration configuration = new HibernateConfiguration();
        configuration.configure();
        EntityManager em = null;
        try (SessionFactory factory = configuration.getFactory()) {
            em = factory.createEntityManager();
            em.getTransaction().begin();

            try{
                Subject subject = new Subject("78:111", "Street 1");
                Engineer engineer = new Engineer();
                engineer.setLogin("admin");
                em.persist(engineer);
                subject.setEngineer(engineer);
                em.persist(subject);
                em.getTransaction().commit();
            }catch (Exception e){
                em.getTransaction().rollback();
                throw e;
            }
        }finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
