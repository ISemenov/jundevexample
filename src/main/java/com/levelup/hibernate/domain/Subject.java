package com.levelup.hibernate.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Student-28 on 18.01.2019.
 */
@Getter
@Setter
@Entity
@Table(name = "subjects")
public class Subject {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Engineer engineer;

    @Column(unique = true, nullable = false)
    private String cadNum;

    @Column(name = "title")
    private String title;

    @Column(unique = true, nullable = false, length = 500)
    private String address;

    public Subject() {
    }

    public Subject(String cadNum, String address) {
        this.cadNum = cadNum;
        this.address = address;
    }
}
