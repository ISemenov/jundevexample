package com.levelup.hibernate.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Student-28 on 18.01.2019.
 */
@Getter
@Setter
@Entity
public class Engineer {

    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false, unique = true)
    private String login;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Subject> subjects;
}
