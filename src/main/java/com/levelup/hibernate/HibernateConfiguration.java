package com.levelup.hibernate;

import com.levelup.hibernate.domain.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.SessionFactory;

import java.util.Properties;

/**
 * Created by Student-28 on 18.01.2019.
 */
public class HibernateConfiguration {

    private SessionFactory factory;

    public HibernateConfiguration() {
    }

    public void configure(){

        Properties properties = new Properties();
        properties.setProperty("hibernate.archive.autodetection", "class");
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.setProperty("hibernate.connection.driver", "org.hibernate.dialect.H2Dialect");
        properties.setProperty("hibernate.connection.url", "jdbc:h2:./target/h2-2");
        properties.setProperty("hibernate.connection.username", "root");
        properties.setProperty("hibernate.connection.password", "root");
        properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        properties.setProperty("hibernate.show_sql", "true");
        properties.setProperty("hibernate.format_sql", "true");

        ServiceRegistry registry = new StandardServiceRegistryBuilder()
                .applySettings(properties)
                .build();

        Configuration configuration = new Configuration();

        configuration.addAnnotatedClass(Subject.class);
        configuration.addAnnotatedClass(Engineer.class);

        factory = configuration.buildSessionFactory(registry);

    }

    public SessionFactory getFactory() {
        return factory;
    }
}

