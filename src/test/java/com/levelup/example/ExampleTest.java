package com.levelup.example;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

/**
 * Created by Student-28 on 18.01.2019.
 */
public class ExampleTest {

    private Example example = new Example();
    private File temp;

    @Before
    public void prepare() throws Exception{
        example.setValue(777);
        temp = File.createTempFile("test", "test");
    }

    @Test
    public void testSum() throws Exception {
        Assert.assertEquals(3, new Example().sum(1, 2));
    }

    @After
    public void cleanup(){
        if(temp != null){
            temp.delete();
        }
    }

}